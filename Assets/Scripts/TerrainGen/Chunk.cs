﻿//store the data of their contents and create a mesh of their contained voxels for rendering and collisions

using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshCollider))]

public class Chunk : MonoBehaviour {

    public static int chunkSize = 16;
    public Block[, ,] blocks = new Block[chunkSize, chunkSize, chunkSize];
    public bool update = false;
    public bool rendered;

    public World world;
    public WorldPos pos;

    MeshFilter filter;
    MeshCollider colli;
	// Use this for initialization
	void Start () {

        filter = gameObject.GetComponent<MeshFilter>();
        colli = gameObject.GetComponent<MeshCollider>();

	}
	
	// Update is called once per frame
	void Update () {
        if (update)
        {
            update = false;
            UpdateChunk();
        }
	}

    void UpdateChunk() {
        MeshData meshData = new MeshData();
        rendered = true;

        for (int x = 0; x < chunkSize; x++)
        {
            for (int y = 0; y < chunkSize; y++)
            {
                for (int z = 0; z < chunkSize; z++)
                {
                    meshData = blocks[x, y, z].Blockdata(this, x,y,z,meshData);
                }
            }
        }

        RenderMesh(meshData);
    }

    void RenderMesh(MeshData meshData) {
        filter.mesh.Clear();
        filter.mesh.vertices = meshData.vertices.ToArray();
        filter.mesh.triangles = meshData.triangles.ToArray();

        filter.mesh.uv = meshData.uv.ToArray();
        filter.mesh.RecalculateNormals();

        colli.sharedMesh = null;

        Mesh mesh = new Mesh();
        mesh.vertices = meshData.colVertices.ToArray();
        mesh.triangles = meshData.colTriangles.ToArray();
        mesh.RecalculateNormals();

        colli.sharedMesh = mesh;
    }

    public Block getBlock(int x, int y, int z)
    {
        if(inRange(x) && inRange(y) && inRange(z))
            return blocks[x, y, z];
        return world.getBlock(pos.x + x, pos.y + y, pos.z + z);
    }

    //simple check function to see if in bounds
    public static bool inRange(int index) {
        if (index < 0 || index >= chunkSize)
            return false;
        return true;
    }

    public void setBlock(int x, int y, int z, Block block) {
        if (inRange(x) && inRange(y) && inRange(z)) {
            blocks[x, y, z] = block;
        }
        else {
            world.setBlock(pos.x + x, pos.y + y, pos.z + z, block);
        }
    }

    public void setBlocksUnmodified() {
        foreach(Block block in blocks) {
            block.changed = false;
        }
    }
}
