﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class Block_Air : Block {

    public Block_Air() : base()
    {

    }

    public override MeshData Blockdata(Chunk chunk, int x, int y, int z, MeshData meshData)
    {
        return meshData;
    }

    public override bool isSolid(Block.Direction dir)
    {
        return false;
    }
}
