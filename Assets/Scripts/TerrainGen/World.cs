﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class World : MonoBehaviour {

    public GameObject chunkPrefab;
    public Dictionary<WorldPos, Chunk> chunks = new Dictionary<WorldPos, Chunk>();
    public string worldName = "world";

    
    void updateIfEqual(int val1, int val2, WorldPos pos) {
        if (val1 == val2) {
            Chunk chunk = getChunk(pos.x, pos.y, pos.z);
            if (chunk != null)
                chunk.update = true;
        }
    }

    public void CreateChunk(int x, int y, int z) {
        WorldPos worldPos = new WorldPos(x, y, z);

        //instantiate chunk at x,y,z
        GameObject newChunkObj = Instantiate(chunkPrefab, 
            new Vector3(x, y, z), Quaternion.Euler(Vector3.zero)
            ) as GameObject;
        //get the obj's chunk component
        Chunk newChunk = newChunkObj.GetComponent<Chunk>();

        newChunk.pos = worldPos;
        newChunk.world = this;

        //add to dictionary
        chunks.Add(worldPos, newChunk);

        var terrainGen = new TerrainGen();
        newChunk = terrainGen.chunkGen(newChunk);

        newChunk.setBlocksUnmodified();
        bool loaded = Serialization.loadChunk(newChunk);

        
    }

    public Chunk getChunk(int x, int y, int z) {
        WorldPos pos = new WorldPos();
        float multiple = Chunk.chunkSize;
        pos.x = Mathf.FloorToInt(x / multiple) * Chunk.chunkSize;
        pos.y = Mathf.FloorToInt(y / multiple) * Chunk.chunkSize;
        pos.z = Mathf.FloorToInt(z / multiple) * Chunk.chunkSize;

        Chunk containerChunk = null;
        chunks.TryGetValue(pos, out containerChunk);

        return containerChunk;
    }

    public Block getBlock(int x, int y, int z) {
        Chunk containerChunk = getChunk(x, y, z);

        if (containerChunk != null) {
            Block block = containerChunk.getBlock(
                x - containerChunk.pos.x,
                y - containerChunk.pos.y,
                z - containerChunk.pos.z);
            return block;
        }
        else {
            return new Block_Air();
        }
    }

    public void setBlock(int x, int y, int z, Block block) {
        Chunk chunk = getChunk(x, y, z);

        if (chunk != null) {
            chunk.setBlock(x - chunk.pos.x, y - chunk.pos.y, z - chunk.pos.z, block);
            chunk.update = true;
        }

        //check if neighboring chunk needs updating
        updateIfEqual(x - chunk.pos.x, 0, new WorldPos(x - 1, y, z));
        updateIfEqual(x - chunk.pos.x, Chunk.chunkSize - 1, new WorldPos(x + 1, y, z));
        updateIfEqual(y - chunk.pos.y, 0, new WorldPos(x, y - 1, z));
        updateIfEqual(y - chunk.pos.y, Chunk.chunkSize - 1, new WorldPos(x, y + 1, z));
        updateIfEqual(z - chunk.pos.z, 0, new WorldPos(x, y, z - 1));
        updateIfEqual(z - chunk.pos.z, Chunk.chunkSize - 1, new WorldPos(x, y, z + 1));
    }

    public void destroyChunk(int x, int y, int z) {
        Chunk chunk = null;
        if (chunks.TryGetValue(new WorldPos(x, y, z), out chunk)) {
            Serialization.saveChunk(chunk);
            Object.Destroy(chunk.gameObject);
            chunks.Remove(new WorldPos(x, y, z));
        }
    }
}
