﻿//provide us with an easy way to store mesh data.
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MeshData {

    public List<Vector3> vertices = new List<Vector3>();
    //every 3 entires = 1 triangle
    public List<int> triangles = new List<int>();
    //list of texture coords(2 entries per triangle)
    public List<Vector2> uv = new List<Vector2>();

    //collider mesh(can make diff mesh for render/collider)
    public List<Vector3> colVertices = new List<Vector3>();
    public List<int> colTriangles = new List<int>();

    public bool useRenderDataforCol;

    public void addQuadTriangles() {
        //vertices added in CW order forming 2 triangles for each face
        triangles.Add(vertices.Count - 4);
        triangles.Add(vertices.Count - 3);
        triangles.Add(vertices.Count - 2);

        triangles.Add(vertices.Count - 4);
        triangles.Add(vertices.Count - 2);
        triangles.Add(vertices.Count - 1);

        if (useRenderDataforCol)
        {
            colTriangles.Add(colVertices.Count - 4);
            colTriangles.Add(colVertices.Count - 3);
            colTriangles.Add(colVertices.Count - 2);

            colTriangles.Add(colVertices.Count - 4);
            colTriangles.Add(colVertices.Count - 2);
            colTriangles.Add(colVertices.Count - 1);
        }
    }

    public void addVertex(Vector3 v3) {
        vertices.Add(v3);

        if (useRenderDataforCol) {
            colVertices.Add(v3);
        }
    }

    public void addTriangle(int tri) {
        triangles.Add(tri);

        if (useRenderDataforCol) {
            colTriangles.Add(tri - (vertices.Count - colVertices.Count));
        }
    }
}
