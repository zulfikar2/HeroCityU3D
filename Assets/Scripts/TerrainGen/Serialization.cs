﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

public static class Serialization {

    public static string saveFolderName = "Saves";

    public static string saveLocation(string worldName) {
        string saveLocation = saveFolderName + "/" + worldName + "/";
        if (!Directory.Exists(saveLocation)) {
            Directory.CreateDirectory(saveLocation);
        }
        return saveLocation;
    }

    public static string fileName(WorldPos chunkLoc) {

        string fileName = chunkLoc.x + "," + chunkLoc.y + "," + chunkLoc.z + ".bin";
        return fileName;
    }

    public static void saveChunk(Chunk chunk) {

        Save save = new Save(chunk);
        if (save.blocks.Count == 0)
            return;

        string saveFile = saveLocation(chunk.world.worldName);
        saveFile += fileName(chunk.pos);

        IFormatter formatter = new BinaryFormatter();
        Stream stream = new FileStream(saveFile, FileMode.Create, FileAccess.Write, FileShare.None);
        formatter.Serialize(stream, save);
        stream.Close();
    }

    public static bool loadChunk(Chunk chunk) {

        string saveFile = saveLocation(chunk.world.worldName);
        saveFile += fileName(chunk.pos);
        if (!File.Exists(saveFile))
            return false;
        IFormatter formatter = new BinaryFormatter();
        FileStream stream = new FileStream(saveFile, FileMode.Open);

        Save save = (Save)formatter.Deserialize(stream);
        foreach (var Block in save.blocks) {
            chunk.blocks[Block.Key.x, Block.Key.y, Block.Key.z] = Block.Value;
        }
        stream.Close();
        return true;
    }
}
