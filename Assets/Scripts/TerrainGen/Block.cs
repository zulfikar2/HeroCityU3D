﻿//contains information about one block and how it should be rendered
using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class Block {

    public bool changed = true;

    public enum Direction { north, east, south, west, up, down };

    const float tileSize = 0.25f;
    public struct Tile {
        public int x; 
        public int y;
    };

    public Block() {

    }
    //block can fill out it's own meshdata
    public virtual MeshData Blockdata
        (Chunk chunk, int x, int y, int z, MeshData meshData){

            meshData.useRenderDataforCol = true;
            if (!chunk.getBlock(x, y + 1, z).isSolid(Direction.down))
            {
                meshData = faceDataUp(chunk, x, y, z, meshData);
            }
            if (!chunk.getBlock(x, y - 1, z).isSolid(Direction.down))
            {
                meshData = faceDataDown(chunk, x, y, z, meshData);
            }
            if (!chunk.getBlock(x, y, z + 1).isSolid(Direction.down))
            {
                meshData = faceDataNorth(chunk, x, y, z, meshData);
            }
            if (!chunk.getBlock(x, y, z - 1).isSolid(Direction.down))
            {
                meshData = faceDataSouth(chunk, x, y, z, meshData);
            }
            if (!chunk.getBlock(x + 1, y, z).isSolid(Direction.down))
            {
                meshData = faceDataEast(chunk, x, y, z, meshData);
            }
            if (!chunk.getBlock(x - 1, y, z).isSolid(Direction.down))
            {
                meshData = faceDataWest(chunk, x, y, z, meshData);
            }
            return meshData;
    }

    protected virtual MeshData faceDataUp
        (Chunk chunk, int x, int y, int z, MeshData meshData) {
        meshData.addVertex(new Vector3(x - 0.5f, y + 0.5f, z + 0.5f));
        meshData.addVertex(new Vector3(x + 0.5f, y + 0.5f, z + 0.5f));
        meshData.addVertex(new Vector3(x + 0.5f, y + 0.5f, z - 0.5f));
        meshData.addVertex(new Vector3(x - 0.5f, y + 0.5f, z - 0.5f));

        meshData.addQuadTriangles();
        meshData.uv.AddRange(faceUV(Direction.up));
        return meshData;
    }

    protected virtual MeshData faceDataDown
        (Chunk chunk, int x, int y, int z, MeshData meshData) {
        meshData.addVertex(new Vector3(x - 0.5f, y - 0.5f, z - 0.5f));
        meshData.addVertex(new Vector3(x + 0.5f, y - 0.5f, z - 0.5f));
        meshData.addVertex(new Vector3(x + 0.5f, y - 0.5f, z + 0.5f));
        meshData.addVertex(new Vector3(x - 0.5f, y - 0.5f, z + 0.5f));

        meshData.addQuadTriangles();
        meshData.uv.AddRange(faceUV(Direction.down));
        return meshData;
    }

    protected virtual MeshData faceDataNorth
        (Chunk chunk, int x, int y, int z, MeshData meshData) {
        meshData.addVertex(new Vector3(x + 0.5f, y - 0.5f, z + 0.5f));
        meshData.addVertex(new Vector3(x + 0.5f, y + 0.5f, z + 0.5f));
        meshData.addVertex(new Vector3(x - 0.5f, y + 0.5f, z + 0.5f));
        meshData.addVertex(new Vector3(x - 0.5f, y - 0.5f, z + 0.5f));

        meshData.addQuadTriangles();
        meshData.uv.AddRange(faceUV(Direction.north));
        return meshData;
    }

    protected virtual MeshData faceDataEast
        (Chunk chunk, int x, int y, int z, MeshData meshData)
    {
        meshData.addVertex(new Vector3(x + 0.5f, y - 0.5f, z - 0.5f));
        meshData.addVertex(new Vector3(x + 0.5f, y + 0.5f, z - 0.5f));
        meshData.addVertex(new Vector3(x + 0.5f, y + 0.5f, z + 0.5f));
        meshData.addVertex(new Vector3(x + 0.5f, y - 0.5f, z + 0.5f));

        meshData.addQuadTriangles();
        meshData.uv.AddRange(faceUV(Direction.east));
        return meshData;
    }

    protected virtual MeshData faceDataSouth
        (Chunk chunk, int x, int y, int z, MeshData meshData)
    {
        meshData.addVertex(new Vector3(x - 0.5f, y - 0.5f, z - 0.5f));
        meshData.addVertex(new Vector3(x - 0.5f, y + 0.5f, z - 0.5f));
        meshData.addVertex(new Vector3(x + 0.5f, y + 0.5f, z - 0.5f));
        meshData.addVertex(new Vector3(x + 0.5f, y - 0.5f, z - 0.5f));

        meshData.addQuadTriangles();
        meshData.uv.AddRange(faceUV(Direction.south));
        return meshData;
    }

    protected virtual MeshData faceDataWest
        (Chunk chunk, int x, int y, int z, MeshData meshData)
    {
        meshData.addVertex(new Vector3(x - 0.5f, y - 0.5f, z + 0.5f));
        meshData.addVertex(new Vector3(x - 0.5f, y + 0.5f, z + 0.5f));
        meshData.addVertex(new Vector3(x - 0.5f, y + 0.5f, z - 0.5f));
        meshData.addVertex(new Vector3(x - 0.5f, y - 0.5f, z - 0.5f));

        meshData.addQuadTriangles();
        meshData.uv.AddRange(faceUV(Direction.west));
        return meshData;
    }

    public virtual bool isSolid(Direction dir)
    {
        switch (dir)
        {
            case Direction.north:
                return true;
            case Direction.east:
                return true;
            case Direction.south:
                return true;
            case Direction.west:
                return true;
            case Direction.up:
                return true;
            case Direction.down:
                return true;
        }
        return false;
    }

    public virtual Tile texturePosition(Direction direction) {
        Tile tile = new Tile();
        tile.x = 0;
        tile.y = 0;
        return tile;
    }

    public virtual Vector2[] faceUV(Direction direction) {
        Vector2[] UV = new Vector2[4];
        Tile tilePos = texturePosition(direction);

        UV[0] = new Vector2(tileSize * tilePos.x + tileSize, tileSize * tilePos.y);
        UV[1] = new Vector2(tileSize * tilePos.x + tileSize, tileSize * tilePos.y + tileSize);
        UV[2] = new Vector2(tileSize * tilePos.x, tileSize * tilePos.y + tileSize);
        UV[3] = new Vector2(tileSize * tilePos.x, tileSize * tilePos.y);

        return UV;
    }
}