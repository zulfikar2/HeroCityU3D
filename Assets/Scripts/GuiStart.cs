﻿using UnityEngine;
using System.Collections;

public class GuiStart : MonoBehaviour {
    private const int BUTTON_WIDTH = 100;
    private const int BUTTON_HEIGHT = 50;
    private const int OFFSET = BUTTON_HEIGHT + BUTTON_HEIGHT/2;

    public GUISkin GUI_Skin;
    public GUIStyle GUI_Style;

	// Use this for initialization
	void Start () {
        //maybe set up city? preload?
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnGUI() {
        GUI.skin = GUI_Skin;
        DisplayMenu();
    }

    private void DisplayMenu() {

        if (GUI.Button(new Rect(50, 0, BUTTON_WIDTH, BUTTON_HEIGHT), "Play"))
        {
            //start game
            Debug.Log("Starting Game...");
            Application.LoadLevel("Game");
        }
        if (GUI.Button(new Rect(50, OFFSET, BUTTON_WIDTH, BUTTON_HEIGHT), "Options"))
        {
            //open options menu
            Debug.Log("Opening options...");
        }
        if (GUI.Button(new Rect(50, OFFSET*2, BUTTON_WIDTH, BUTTON_HEIGHT), "Help"))
        {
            //maybe help pop up window?
        }
        if (GUI.Button(new Rect(50, OFFSET*3, BUTTON_WIDTH, BUTTON_HEIGHT), "Exit"))
        {
            //close
            Debug.Log("Exiting..");
            Application.Quit();
        }
    }
}
